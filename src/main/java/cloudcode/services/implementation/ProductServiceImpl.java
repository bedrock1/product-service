package cloudcode.services.implementation;

import cloudcode.models.Product;
import cloudcode.repositories.ProductRepository;
import cloudcode.services.ProductService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        return (ArrayList) productRepository.findAll();
    }

    @Override
    public Product findOneById(ObjectId id) {
        return productRepository.findOne(id);
    }

    @Override
    public List<Product> findByIdIn(List<ObjectId> ids) {
        return productRepository.findByIdIn(ids);
    }
}
