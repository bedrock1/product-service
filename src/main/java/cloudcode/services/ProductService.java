package cloudcode.services;

import org.springframework.stereotype.Component;
import cloudcode.models.Product;
import org.bson.types.ObjectId;

import java.util.List;

@Component
public interface ProductService {

    List<Product> findAll();
    Product findOneById(ObjectId id);
    List<Product> findByIdIn(List<ObjectId> ids);
}
