package cloudcode.repositories;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;
import cloudcode.models.Product;

import java.util.List;

public interface ProductRepository extends PagingAndSortingRepository<Product, ObjectId> {

    List<Product> findByIdIn(List<ObjectId> ids);
}
