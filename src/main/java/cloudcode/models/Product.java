package cloudcode.models;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "products")
public class Product {

    private String  id;
    private String  productId;
    private String  make;
    private String  name;
    private String  code;
    private String  deviceOS;
    private Boolean available;
    private Boolean preOwned;
    private Boolean swapEligible;
    private String  seoName;
    private Number  sequence;
    private String  imageUrl;
    private Number  retailPrice;
    private Number  discountedPrice;
    private Number  monthlyPrice;
}
