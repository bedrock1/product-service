package cloudcode.models;

public class ProductDetail {

    private String  id;
    private String  sku;
    private String  storage;
    private String  color;
    private String  condition;
    private Boolean inStock;
    private Number  oneTimePrice;
    private Number  discountedPrice;
    private Number  monthlyPrice;
    private Number  numberMonths;
}
