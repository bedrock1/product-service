package cloudcode.controllers;

import cloudcode.models.Product;
import cloudcode.services.ProductService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/")
    public List<Product> getAllProducts() {
        return productService.findAll();
    }

    @GetMapping(value = "/{productId}")
    public Product getProduct(@PathVariable("productId")ObjectId productId) {
        return productService.findOneById(productId);
    }
}
