package cloudcode.datafetchers;

import cloudcode.models.Product;
import cloudcode.services.ProductService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductDataFetcher implements DataFetcher<List<Product>> {

    private final ProductService productService;

    @Autowired
    ProductDataFetcher(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public List<Product> get(DataFetchingEnvironment environment) {
        List<Product>  products = productService.findAll();
        return products;
    }
}
