# Start with a base image containing Java runtime
FROM openjdk:8u212-jdk-slim

# Maintainer Info
MAINTAINER Jaydeep Dave <jaydeep.dave@visible.com>

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available for the world outside this container
EXPOSE 8080

# Application's JAR file
ARG JAR_FILE=./target/product-service-1.0.0.jar

RUN echo $JAR_FILE

# Copy Application JAR to the container
ADD $JAR_FILE product-service.jar

# Run the Application
ENTRYPOINT ["java", "-jar", "product-service.jar"]